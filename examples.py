# looping over a range of numbers
for i in [0, 1, 2, 3, 4, 5]:
    print(i**2)

for i in range(6):
    print(i**2)

# looping backwards
for i in reversed(range(6)):
    print(i**2)

# looping over two collections
names = ['raymond', 'rachel', 'matthew']
colors = ['red', 'green', 'blue', 'yellow']

n = min(len(names), len(colors))
for i in range(n):
    print(names[i], '-->', colors[i])

for k, v in zip(names, colors):
    print(k, '-->', v)

# looping over a dictionary keys and values
# slow
d = {'matthew': 'blue', 'rachel': 'green'}
for k in d:
    print(k, '-->', d[k])
#fast
for k, v in d.items():
    print(k, '-->', d[k])

# distinguishing multiple exit points in loops
def find(seq, target):
    found = False
    for i, value in enumerate(seq):
        if value == target:
            found = True
            break
    if not found:
        return -1
    return i

def find_good(seq, target):
    for i, value in enumerate(seq):
        if value == target:
            break
    else:
        return -1
    return i

# if you need to edit key while iterating
d = {'matthew': 'blue', 'rachel': 'green'}
d = {k : d[k] for k in d if not k.startswith('r')}
print(d)

# construct a dictionary from pairs
names = ['raymond', 'rachel', 'matthew']
colors = ['red', 'green', 'blue', 'yellow']

d = dict(zip(names, colors))

# concatenating strings
print(', '.join(names))

# counting with dictionaries
d = {}
for color in colors:
    d[color] = d.get(color, 0) + 1
